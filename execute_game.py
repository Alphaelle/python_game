from custom_item import Cannon, RESOLUTION
from pyglet.window import key
from game_engine import init, Game, Layer

resolution = RESOLUTION
init(resolution)

game= Game()
#game.debug= True

layer = Layer()
game.add(layer)

cannon1 = Cannon (
    r'assets/cannon.png', 
    position=(0, 0) ,
    bullet_origin= (65, 65) , 
    bullet_sens=(1, 1), 
    trigger_key= key.A) 

layer.add(cannon1)

cannon2 = Cannon (
    r'assets/cannon2.png', 
    position=(740,0) ,
    bullet_origin= (725, 65) , 
    bullet_sens=(-2, 1), 
    trigger_key= key.P) 

layer.add(cannon2)


game.run()